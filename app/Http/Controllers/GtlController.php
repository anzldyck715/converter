<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GtlController extends Controller
{
    public function newParticipant(Request $request)
    {
        return view('pages.gtl.new_participant');
    }

    public function deleteParticipant(Request $request)
    {
        return view('pages.gtl.delete_participant');
    }

    public function mpp(Request $request)
    {
        return view('pages.gtl.mpp');
    }

    public function increase(Request $request)
    {
        return view('pages.gtl.increase');
    }

    public function decrease(Request $request)
    {
        return view('pages.gtl.decrease');
    }

    public function endorsement(Request $request)
    {
        return view('pages.gtl.endorsement');
    }

    public function kepesertaan(Request $request)
    {
        return view('pages.gtl.kepesertaan');
    }
}
