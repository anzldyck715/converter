<?php

namespace App\Http\Controllers;

use App\Exports\ReportSlaSpaj;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class IndividuController extends Controller
{
    public function policyStatus(Request $request)
    {
        return view('pages.individu.policy_status');
    }

    public function policyJatuhTempo(Request $request)
    {
        return view('pages.individu.policy_due');
    }

    public function smsJatuhTempo(Request $request)
    {
        return view('pages.individu.sms_due');
    }

    public function smsDebetRekening(Request $request)
    {
        return view('pages.individu.sms_debet_rekening');
    }

    public function smsDebetKartuKredit(Request $request)
    {
        return view('pages.individu.sms_debet_kredit');
    }

    public function smsKonfirmasiPembayaran(Request $request)
    {
        return view('pages.individu.sms_pembayaran');
    }

    public function policyInformation(Request $request)
    {
        $dataFound = false;
        return view('pages.individu.policy_information', compact('dataFound'));
    }

    public function getPolicyInformation(Request $request)
    {
        $request->validate([
            'policyNumber' => 'nullable|string',
            'name' => 'nullable|string',
            'dob' => 'nullable|date',
            'premi' => 'nullable|numeric',
        ]);
    
        // Inisialisasi query builder
        $query = Policy::query();
    
        // Lakukan pengecekan parameter yang ada dan tambahkan kondisi ke dalam query
        if ($request->filled('policyNumber')) {
            $query->where('policy_number', $request->policyNumber);
        }
    
        if ($request->filled('name')) {
            $query->where('name', $request->name);
        }
    
        if ($request->filled('dob')) {
            $query->where('date_of_birth', $request->dob);
        }
    
        if ($request->filled('premi')) {
            $query->where('total_premi', $request->premi);
        }
    
        // Jalankan query untuk mendapatkan hasil
        $policies = $query->get();
    
        if ($policies->isNotEmpty()) {
            // Jika data ditemukan, Anda bisa melakukan sesuatu di sini
            return view('policy.show', compact('policies'));
        } else {
            // Jika data tidak ditemukan, kembalikan pesan error atau melakukan sesuatu yang sesuai
            return redirect()->back()->with('error', 'No policies found with the provided criteria.');
        }
    }

    public function jtAutoDebet(Request $request)
    {
        return view('pages.individu.jt_autodebet');
    }

    public function redemptionTax(Request $request)
    {
        return view('pages.individu.redemption_tax');
    }

    public function redemptionAll(Request $request)
    {
        return view('pages.individu.redemption_all');
    }

    public function pos(Request $request)
    {
        return view('pages.individu.pos');
    }

    public function jtTahapan(Request $request)
    {
        return view('pages.individu.jt_tahapan');
    }

    public function slaSpaj(Request $request)
    {
        return view('pages.individu.sla_spaj');
    }

    public function reportSlaSpaj(Request $request)
    {
        return Excel::download(new ReportSlaSpaj(), 'slaSpajList.xlxs');
    }

    public function collectedCallcenter(Request $request)
    {
        return view('pages.individu.collected_callcenter');
    }

    public function accountingPremiumNb(Request $request)
    {
        return view('pages.individu.accounting_premium_nb');
    }

    public function accountingPremiumRenewal(Request $request)
    {
        return view('pages.individu.accounting_premium_rn');
    }
}
