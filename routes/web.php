<?php

use App\Http\Controllers\AjkController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GtlController;
use App\Http\Controllers\IndividuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::get('/dashboard', [DashboardController::class,'index'])->middleware(['auth'])->name('dashboard');
Route::get('/refund-checker', [AjkController::class,'refundChecker'])->middleware(['auth'])->name('refund.checker');
Route::get('/new-participant', [GtlController::class,'newParticipant'])->middleware(['auth'])->name('new.participant');
Route::get('/delete-participant', [GtlController::class,'deleteParticipant'])->middleware(['auth'])->name('delete.participant');
Route::get('/mpp', [GtlController::class,'mpp'])->middleware(['auth'])->name('mpp');
Route::get('/increase', [GtlController::class,'increase'])->middleware(['auth'])->name('increase');
Route::get('/decrease', [GtlController::class,'decrease'])->middleware(['auth'])->name('decrease');
Route::get('/endorsement', [GtlController::class,'endorsement'])->middleware(['auth'])->name('endorsement');
Route::get('/kepesertaan', [GtlController::class,'kepesertaan'])->middleware(['auth'])->name('kepesertaan');
Route::get('/policy-status', [IndividuController::class,'policyStatus'])->middleware(['auth'])->name('policy.status');
Route::get('/policy-due', [IndividuController::class,'policyJatuhTempo'])->middleware(['auth'])->name('policy.due');
Route::get('/sms-due', [IndividuController::class,'smsJatuhTempo'])->middleware(['auth'])->name('sms.due');
Route::get('/sms-rekening', [IndividuController::class,'smsDebetRekening'])->middleware(['auth'])->name('sms.rekening');
Route::get('/sms-kredit', [IndividuController::class,'smsDebetKartuKredit'])->middleware(['auth'])->name('sms.kredit');
Route::get('/sms-pembayaran', [IndividuController::class,'smsKonfirmasiPembayaran'])->middleware(['auth'])->name('sms.pembayaran');
Route::get('/policy-information', [IndividuController::class,'policyInformation'])->middleware(['auth'])->name('policy.information');
Route::post('/policy-information', [IndividuController::class,'getPolicyInformation'])->middleware(['auth'])->name('get.policy.information');
Route::get('/jt-autodebet', [IndividuController::class,'jtAutoDebet'])->middleware(['auth'])->name('jt.debet');
Route::get('/redemption-tax', [IndividuController::class,'redemptionTax'])->middleware(['auth'])->name('redemption.tax');
Route::get('/redemption-all', [IndividuController::class,'redemptionAll'])->middleware(['auth'])->name('redemption.all');
Route::get('/pos', [IndividuController::class,'pos'])->middleware(['auth'])->name('pos');
Route::get('/jt-tahapan', [IndividuController::class,'jtTahapan'])->middleware(['auth'])->name('jt.tahapan');
Route::get('/sla-spaj', [IndividuController::class,'slaSpaj'])->middleware(['auth'])->name('sla.spaj');
Route::post('/sla-spaj', [IndividuController::class,'reportSlaSpaj'])->middleware(['auth'])->name('report.sla.spaj');
Route::get('/collected-callcenter', [IndividuController::class,'collectedCallcenter'])->middleware(['auth'])->name('collected.callcenter');
Route::get('/accounting-premium-nb', [IndividuController::class,'accountingPremiumNb'])->middleware(['auth'])->name('accounting.premium.nb');
Route::get('/accounting-premium-renewal', [IndividuController::class,'accountingPremiumRenewal'])->middleware(['auth'])->name('accounting.premium.rn');

require __DIR__.'/auth.php';
