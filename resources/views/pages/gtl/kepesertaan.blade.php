@extends('layouts.dashboard')

@section('header-content')
<div class="d-flex flex-column flex-row-fluid">
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('dashboard') }}" class="text-white text-hover-primary">
                    <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Kepesertaan</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    
</div>
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Row-->
    <div class="row g-5 g-xl-12">
        <!--begin::Col-->
        <div class="col-xl-12">
            <!--begin::Misc Widget 1-->
            <div class="row mb-5 mb-xl-8 g-5 g-xl-12">
                <!--begin::Col-->
                <div class="col-12">
                    <!--begin::Card-->
                    <div class="card  text-start w-100 text-gray-800 text-hover-primary p-10" href="account/overview.html">
                        
                        <form action="" method="post" enctype="multipart/form-data">
                            @method('POST')
                            @csrf
                            <form action="" method="post" enctype="multipart/form-data">
                                @method('POST')
                                @csrf
                                <div class="form-group row">
                                    <label for="companyname" class="col-2 col-form-label">Company Name</label>
                                    <div class="col-4">
                                      <input type="text" class="form-control" id="companyname" placeholder="Company Name">
                                    </div>
                                </div>
                                <div class="form-group row mt-3">
                                    <label for="policyholder" class="col-2 col-form-label">Policy Holder</label>
                                    <div class="col-4">
                                      <select name="" id="" class="form-control" id="policyholder">
                                        <option value="">1</option>
                                        <option value="">2</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group row mt-3">
                                    <label for="policyholder" class="col-2 col-form-label">Status Peserta</label>
                                    <div class="col-4">
                                      <select name="" id="" class="form-control" id="policyholder">
                                        <option value="">All</option>
                                        <option value="">Active</option>
                                        <option value="">Terminate</option>
                                      </select>
                                    </div>
                                </div>
                               
                                <div class="form-group row mt-3">
                                    <div class="col-2"></div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-flex btn-primary mt-5 btn-sm btn-block">
                                             Search
                                        </button>
                                    </div>
                                </div>
                                
                                
                                
                               
                            </form>
                            
                           
                        </form>
                       
                    </div>
                    <!--end::Card-->
                </div>
               
               
            </div>
           
        
        </div>
    </div>
    <!--end::Row-->
</div>
@endsection