@extends('layouts.dashboard')

@section('header-content')
<div class="d-flex flex-column flex-row-fluid">
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('dashboard') }}" class="text-white text-hover-primary">
                    <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                </a>
            </li>
     
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            <li class="breadcrumb-item text-white fw-bold lh-1">Individu</li>
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            <li class="breadcrumb-item text-white fw-bold lh-1">View</li>
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
          
            <li class="breadcrumb-item text-white fw-bold lh-1">Policy Information</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    
</div>
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Row-->
    <div class="row g-5 g-xl-12">
        <!--begin::Col-->
        <div class="col-xl-12">
            <!--begin::Misc Widget 1-->
            <div class="row mb-5 mb-xl-8 g-5 g-xl-12">
                <!--begin::Col-->
                <div class="col-12">
                    <!--begin::Card-->
                    <div class="card  text-start w-100 text-gray-800 text-hover-primary p-10" href="#">
                        
                        <form action="{{ route('get.policy.information') }}" method="post" enctype="multipart/form-data">
                            @method('POST')
                            @csrf
                            <div class="form-group row">
                                <label for="policyNumber" class="col-2 col-form-label">Policy Number</label>
                                <div class="col-4">
                                  <input type="text" class="form-control" id="policyNumber" name="policyNumber" placeholder="Policy Number">
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <label for="name" class="col-2 col-form-label">Name</label>
                                <div class="col-4">
                                  <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                </div>
                            </div>
                            
                            <div class="form-group row mt-3">
                                <label for="dob" class="col-2 col-form-label">Date of Birth</label>
                                <div class="col-4">
                                  <input type="date" class="form-control" id="dob" name="dob">
                                </div>
                            </div>
                            <div class="form-group row mt-3">
                                <label for="premi" class="col-2 col-form-label">Total Premi</label>
                                <div class="col-4">
                                  <input type="text" class="form-control" id="premi" name="premi" placeholder="Premi">
                                </div>
                            </div>
                           
                            <div class="form-group row mt-3">
                                <div class="col-2"></div>
                                <div class="col-4">
                                    <button type="submit" id="submit" name="submit" class="btn btn-flex btn-primary mt-5 btn-sm btn-block">
                                         Search
                                    </button>
                                </div>
                            </div>
                            
                            
                            
                           
                        </form>
                       
                    </div>
                    <!--end::Card-->
                </div>
               
               
            </div>

           
           
        
        </div>

        @if ($dataFound)
        <div class="row" id="information" hidden>

            <div class="col-xl-6">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Policy Information</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">No Polis</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">023232</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Pemegang Polis</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <span class="fw-semibold text-gray-800 fs-6">Keenthemes</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Tertanggung 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Phone number must be active">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 d-flex align-items-center">
                                <span class="fw-bold fs-6 text-gray-800 me-2">044 3276 454 935</span>
                                <span class="badge badge-success">Verified</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Tgl Mulai Asuransi</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <a href="#" class="fw-semibold fs-6 text-gray-800 text-hover-primary">keenthemes.com</a>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Frekuensi Bayar 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Country of origination">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Germany</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Premi Pokok (Basic/Reader)</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Email, Phone</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Premi Topup</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Premi Sukarela</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Status Polis</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-7">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Cara Bayar</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Notice-->
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">NB Information</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">No Applikasi</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">023232</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">No SPAJ</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <span class="fw-semibold text-gray-800 fs-6">Keenthemes</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Usia Tertanggung 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Phone number must be active">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 d-flex align-items-center">
                                <span class="fw-bold fs-6 text-gray-800 me-2">044 3276 454 935</span>
                                <span class="badge badge-success">Verified</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Medical 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Country of origination">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Germany</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Tgl Akseptasi</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Agent</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Kantor</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-7">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Agent Servicing</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Autodebet Information - Alamat Korespondensi</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Alamat 1</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">023232</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Alamat 2</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <span class="fw-semibold text-gray-800 fs-6">Keenthemes</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Kota 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Phone number must be active">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 d-flex align-items-center">
                                <span class="fw-bold fs-6 text-gray-800 me-2">044 3276 454 935</span>
                                <span class="badge badge-success">Verified</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Kode Pos 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Country of origination">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Germany</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        
                        <!--begin::Input group-->
                        <div class="row mb-7">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Provinsi</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Pemegang Polis</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Nama</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">023232</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">DOB</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <span class="fw-semibold text-gray-800 fs-6">Keenthemes</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Alamat 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Phone number must be active">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 d-flex align-items-center">
                                <span class="fw-bold fs-6 text-gray-800 me-2">044 3276 454 935</span>
                                <span class="badge badge-success">Verified</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Kota - Kode Pos</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <a href="#" class="fw-semibold fs-6 text-gray-800 text-hover-primary">keenthemes.com</a>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Provinsi 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Country of origination">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Germany</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Handphone</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Email, Phone</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Email</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Bank</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Cabang</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Nama Pemilik Rekening</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-7">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Nomor Rekening</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Notice-->
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Tertanggung</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Nama</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">023232</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Row-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">DOB</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <span class="fw-semibold text-gray-800 fs-6">Keenthemes</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Alamat 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Phone number must be active">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 d-flex align-items-center">
                                <span class="fw-bold fs-6 text-gray-800 me-2">044 3276 454 935</span>
                                <span class="badge badge-success">Verified</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Kota - Kode Pos 
                            <span class="ms-1" data-bs-toggle="tooltip" title="Country of origination">
                                <i class="ki-outline ki-information fs-7"></i>
                            </span></label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                <span class="fw-bold fs-6 text-gray-800">Germany</span>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        
                        <!--begin::Input group-->
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Provinsi</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-5">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Handphone</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        <div class="row mb-7">
                            <!--begin::Label-->
                            <label class="col-lg-4 fw-semibold text-muted">Email</label>
                            <!--begin::Label-->
                            <!--begin::Label-->
                            <div class="col-lg-8">
                                <span class="fw-semibold fs-6 text-gray-800">Yes</span>
                            </div>
                            <!--begin::Label-->
                        </div>
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Plan Information</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Jenis</th>
                                    <th>Plan Code</th>
                                    <th>Plan Name</th>
                                    <th>Status</th>
                                    <th>Up</th>
                                    <th>Biaya Asuransi (Bulan 1 - Tahun 1)</th>
                                    <th>Masa Pembayaran</th>
                                    <th>Masa Asuransi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="8" class="text-center">No data</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Ahli Waris Information</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Hubungan</th>
                                    <th>Persentasi</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3" class="text-center">No data</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Jadwal - Jatuh Tempo Tahapan</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>JT Tahapan</th>
                                    <th>Nama Tahapan</th>
                                    <th>Persentasi</th>
                                    <th>Up</th>
                                    <th>Amount</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="text-center">No data</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">List Jatuh Tempo / Pembayaran</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Tgl Jatuh Tempo</th>
                                    <th>Tgl Pembayaran</th>
                                    <th>No. Rek</th>
                                    <th>Premi</th>
                                    <th>Bunga</th>
                                    <th>Materai</th>
                                    <th>Biaya</th>
                                    <th>Total Uang Masuk</th>
                                    <th>Selisih</th>
                                    <th>PH</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="10" class="text-center">No data</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-xl-12">
                <div class="card mb-5 mb-xl-10 g-xl-6" id="kt_profile_details_view">
                    <!--begin::Card header-->
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Claim Information</h3>
                        </div>
                       
                    </div>
                    <!--begin::Card header-->
                    <!--begin::Card body-->
                    <div class="card-body p-9">
                        <!--begin::Row-->
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th>Claim Number</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Tgl Pengajuan</th>
                                    <th>RS</th>
                                    <th>Amount Claimed</th>
                                    <th>Amount Payed</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7" class="text-center">No data</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        
                       
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
        </div>
        @endif
        
    </div>
    <!--end::Row-->
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.7.1.slim.js" integrity="sha256-UgvvN8vBkgO0luPSUl2s8TIlOSYRoGFAX4jlCIm9Adc=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        let div = $('#information');
        
       

    })
</script>