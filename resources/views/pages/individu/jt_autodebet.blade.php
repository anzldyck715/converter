@extends('layouts.dashboard')

@section('header-content')
<div class="d-flex flex-column flex-row-fluid">
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('dashboard') }}" class="text-white text-hover-primary">
                    <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Individu</li>
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            <li class="breadcrumb-item text-white fw-bold lh-1">Auto Debet</li>
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            
            <li class="breadcrumb-item text-white fw-bold lh-1">AutoDebet Kartu Kredit - Jatuh Tempo</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    
</div>
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Row-->
    <div class="row g-5 g-xl-12">
        <!--begin::Col-->
        <div class="col-xl-12">
            <!--begin::Misc Widget 1-->
            <div class="row mb-5 mb-xl-8 g-5 g-xl-12">
                <!--begin::Col-->
                <div class="col-12">
                    <!--begin::Card-->
                    <div class="card  text-start w-100 text-gray-800 text-hover-primary p-10" href="#">
                        
                        <form action="" method="post" enctype="multipart/form-data">
                            @method('POST')
                            @csrf
                         
                            
                            <div class="form-group row mt-3">
                                <label for="start" class="col-2 col-form-label">End Period</label>
                                <div class="col-4">
                                  <input type="date" class="form-control" id="start">
                                </div>
                            </div>
                            
                           
                            <div class="form-group row mt-3">
                                <div class="col-2"></div>
                                <div class="col-4">
                                    <button type="submit" class="btn btn-flex btn-primary mt-5 btn-sm btn-block">
                                         Search
                                    </button>
                                </div>
                            </div>
                            
                            
                            
                           
                        </form>
                       
                    </div>
                    <!--end::Card-->
                </div>
               
               
            </div>
           
        
        </div>
    </div>
    <!--end::Row-->
</div>
@endsection