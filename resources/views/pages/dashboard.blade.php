@extends('layouts.dashboard')

@section('header-content')
<div class="d-flex flex-column flex-row-fluid">
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('dashboard') }}" class="text-white text-hover-primary">
                    <i class="ki-outline ki-home text-gray-700 fs-6"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-7 text-gray-700 mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Dashboards</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
    <!--begin::Toolbar wrapper=-->
    <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-13 pb-6">
        <!--begin::Page title-->
        <div class="page-title me-5">
            <!--begin::Title-->
            <h1 class="page-heading d-flex text-white fw-bold fs-2 flex-column justify-content-center my-0">Welcome back, {{ Auth::user()->name }} 
            <!--begin::Description-->
            <span class="page-desc text-gray-600 fw-semibold fs-6 pt-3">Group Term Life - Supporting Application</span>
            <!--end::Description--></h1>
            <!--end::Title-->
        </div>
        <!--end::Page title-->
       
    </div>
    <!--end::Toolbar wrapper=-->
</div>
@endsection

@section('content')
<div id="kt_app_content" class="app-content flex-column-fluid">
    <!--begin::Row-->
    <div class="row g-5 g-xl-8">
        <!--begin::Col-->
        <div class="col-xl-12">
            <!--begin::Misc Widget 1-->
            <div class="row mb-5 mb-xl-8 g-5 g-xl-8">
                <div class="card bgi-position-y-bottom bgi-position-x-end bgi-no-repeat bgi-size-cover min-h-250px bg-body mb-5 mb-xl-8" style="background-position: 100% 50px;background-size: 500px auto;background-image:url('assets/media/misc/city.png')" dir="ltr">
                    <!--begin::Body-->
                    <div class="card-body d-flex flex-column justify-content-center ps-lg-12">
                        <!--begin::Title-->
                        <h3 class="text-gray-900 fs-2qx fw-bold mb-7">Converter
                        <br />Group Term Life - MNC Life</h3>
                        <!--end::Title-->
                        <!--begin::Action-->
                        <div class="m-0">
                            {{-- <a href='#' class="btn btn-dark fw-semibold px-6 py-3" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app">Create a Store</a> --}}
                        </div>
                        <!--begin::Action-->
                    </div>
                    <!--end::Body-->
                </div>
            </div>
           
        
        </div>
       
    </div>
    <!--end::Row-->
</div>
@endsection