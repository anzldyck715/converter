<div id="kt_app_footer" class="app-footer d-flex flex-column flex-md-row align-items-center flex-center flex-md-stack py-2 py-lg-4">
    <div class="text-gray-900 order-2 order-md-1">
        {{-- <span class="text-muted fw-semibold me-1">2024&copy;</span> --}}
        <a href="https://mnclife.com" target="_blank" class="text-gray-800 text-hover-primary">	Copyright © MNC Life Assurance 2024. All rights reserved </a>
    </div>
</div>